from django.urls import path, re_path, include
from django.conf.urls import url
from polls import views
from django.contrib import admin
from .views import *

urlpatterns = [
    #url(r'^$', index, name='index'),
    #url(r'^register$', register, name='register')
    path('', index, name='index'),
    path('index/', index, name='index'),
    path('register/', register, name='register'),
    path('activity/', activity, name='activity'),
    path('hapus_jadwal/', hapus_jadwal, name='hapus_jadwal'),
    url(r'^tambah_jadwal/$', views.tambah_jadwal, name="tambah_jadwal"),
    
]