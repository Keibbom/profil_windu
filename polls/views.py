from django.shortcuts import render, redirect
from .models import Jadwal

# Create your views here.
def index(request):
    response = {}
    return render(request, 'index.html',)

def register(request):
	response = {}
	return render(request, 'challenge.html',)

def activity(request):
	response = {}
	jadwal = Jadwal.objects.all()
	return render(request, 'activity.html',{'Jadwal' : jadwal, 'nbar':'activity'})

def tambah_jadwal(request):
	nama = request.POST["nama"]
	jenis = request.POST["jenis"]
	tempat = request.POST["tempat"]
	tanggal = request.POST["tanggal"]
	waktu = request.POST["waktu"]

	buat_jadwal = Jadwal(
		nama = nama, 
		jenis = jenis, 
		tempat = tempat, 
		tanggal = tanggal, 
		waktu = waktu
	)

	buat_jadwal.save()
	return render(request, 'index.html',{})

def hapus_jadwal(request):
	jadwal = Jadwal.objects.all().delete()
	return render(request, 'activity.html', {'Jadwal' : jadwal})